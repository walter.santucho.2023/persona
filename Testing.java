
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Testing {

    @Test
    public void testEsAdulto()
    {
        LocalDate fechaNacimiento = LocalDate.of(1995, 6, 29);
        Persona esPersona = new Persona("Antonella", fechaNacimiento);
        boolean resultado;
        resultado = esPersona.esAdulto();

        assertEquals(true, resultado);
    }
}
